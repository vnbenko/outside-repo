import UIKit

extension UserDefaults {
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
           let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}


extension UIViewController {
    func alertWrongDeleting () {
        let alert = UIAlertController(title: "Oops!", message: "Your city can't be deleted", preferredStyle: .alert)
        let firstAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(firstAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension Date {
    func getDate(format: String = "dd MM") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    func getDay(format: String = "EEEE") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension UIView {
    func setRadiusWithShadow(radius: CGFloat? = nil, color: CGColor? = nil, position: CGSize? = nil, shadowVolume: CGFloat = 15, opacity: Float = 0.7) {
        self.layer.cornerRadius = radius ?? self.frame.width / 2
        self.layer.shadowColor = color ?? UIColor.black.cgColor
        self.layer.shadowOffset = position ?? CGSize(width: 0, height: 0)
        self.layer.shadowRadius = shadowVolume
        self.layer.shadowOpacity = opacity
        self.layer.masksToBounds = false
    }
    
}

extension UIView {
    func roundCorners(radius: CGFloat = 10) {
        self.layer.cornerRadius = radius
    }
    
}

