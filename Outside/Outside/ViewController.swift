import UIKit
import CoreLocation
import Kingfisher
import AVKit


class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var videoPlayer:AVPlayer?
    var videoPlayerLayer:AVPlayerLayer?
    
    var locationManager = CLLocationManager()
    var coordinate = ""
    var city = ""
    var forecast: WeatherResponse?
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var regionLabel: UILabel!
    @IBOutlet weak var topScreenView: UIView!
    @IBOutlet weak var mainTemperatureLabel: UILabel!
    @IBOutlet weak var weatherDiscriptionLabel: UILabel!
    @IBOutlet weak var weatherPictureImageView: UIImageView!
    @IBOutlet weak var feelsLikeTempLabel: UILabel!
    @IBOutlet weak var precipLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var sunRiseLabel: UILabel!
    @IBOutlet weak var sunSetLabel: UILabel!
    @IBOutlet weak var weatherView: UIView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var additionalView: UIView!
    @IBOutlet weak var middleBackgroundImageView: UIImageView!
    @IBOutlet weak var middleScreenView: UIView!
    @IBOutlet weak var forecastTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getLocation()
        self.getForecast()
        self.makeUIBeauty()
    }
    
    @IBAction func goFavoritesVC(_ sender: UIBarButtonItem) {
        guard let favoritesVC = storyboard?.instantiateViewController(withIdentifier: "FavoritesViewController") as? FavoritesViewController else { return }
        favoritesVC.delegate = self
        self.navigationController?.pushViewController(favoritesVC, animated: true)
    }
    
    func locationManager (_ manager: CLLocationManager,
                          didUpdateLocations locations: [CLLocation]) {
        guard let location: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        self.locationManager.stopUpdatingLocation()
        self.coordinate = "\(location.latitude),\(location.longitude)"
        
        Manager.shared.sendCoordinate(coordinate: coordinate) {
            self.showWeather()
        }
    }
    
    func sendCityRequest () {
        self.checkCity(city: city)
        Manager.shared.sendCity(city: city) {
            self.showWeather()
        }
    }
    
    private func showWeather () {
        DispatchQueue.main.async {
            if let name = Manager.shared.json?.location.name,
               let region = Manager.shared.json?.location.region,
               let country = Manager.shared.json?.location.country,
               let text = Manager.shared.json?.current.condition?.text,
               let icon = Manager.shared.json?.current.condition?.icon?.dropFirst(2),
               let temp_c = Manager.shared.json?.current.temp_c,
               let sunrise = Manager.shared.json?.forecast.forecastday.first?.astro?.sunrise,
               let sunset = Manager.shared.json?.forecast.forecastday.first?.astro?.sunset,
               let humidity = Manager.shared.json?.current.humidity,
               let wind_kph = Manager.shared.json?.current.wind_kph,
               let feelslike_c = Manager.shared.json?.current.feelslike_c,
               let precip_mm = Manager.shared.json?.current.precip_mm {
                
                let url = URL(string: "https://" + icon)
                self.weatherPictureImageView.kf.setImage(with: url)
                self.cityLabel.text = name
                self.regionLabel.text = region
                self.countryLabel.text = country
                self.weatherDiscriptionLabel.text = text
                self.mainTemperatureLabel.text = "\(Int(temp_c))°"
                self.sunRiseLabel.text = sunrise
                self.sunSetLabel.text = sunset
                self.humidityLabel.text = "\(humidity)%"
                self.windSpeedLabel.text = "\(wind_kph) km/h"
                self.feelsLikeTempLabel.text = "Feels like \(Int(feelslike_c))°"
                self.precipLabel.text = "\(precip_mm) mm"
                self.setUpVideo(name: self.getCurrentVideo(definition: text))
                
            }
        }
        self.forecastTableView.reloadData()
        
    }
    
    func setUpVideo(name: String) {
        let video = name
        guard let bundlePath = Bundle.main.path(forResource: video, ofType: "mp4") else { return }
        
        let url = URL(fileURLWithPath: bundlePath)
        let item = AVPlayerItem(url: url)
        
        self.videoPlayer = AVPlayer(playerItem: item)
        self.videoPlayerLayer = AVPlayerLayer(player: videoPlayer!)
        self.videoPlayerLayer?.frame = self.topScreenView.bounds
        self.videoPlayerLayer?.videoGravity = .resizeAspectFill
        self.videoView.layer.addSublayer(videoPlayerLayer!)
        self.videoPlayer?.playImmediately(atRate: 0.5)
        
    }
    
    func getCurrentVideo(definition: String) -> String {
        switch definition.lowercased()  {
        case let text where text.contains("rain"):
            return "rain"
        case let text where text.contains("thunder"):
            return "thunder"
        case let text where text.contains("snow"):
            return "snow"
        case let text where text.contains("cloudy"):
            return "cloudy"
        case let text where text.contains("clear"):
            return "sunny"
        case let text where text.contains("sunny"):
            return "sunny"
        case let text where text.contains("overcast"):
            return "overcast"
        case let text where text.contains("mist"):
            return "fog"
        case let text where text.contains("fog"):
            return "fog"
        default:
            return "normal"
        }
    }
    
    private func getLocation () {
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    private func getForecast() {
        let forecast = UserDefaults.standard.value(WeatherResponse.self, forKey: "weather")
        self.forecast = forecast
    }
    
    private func checkCity(city: String) {
        if city.count >= 2 {
            self.city = city.replacingOccurrences(of: " ", with: "%20")
        } else {
            self.city = city
        }
    }
    
    private func makeUIBeauty() {
        let radius = middleBackgroundImageView.frame.height / 4
        let height = middleBackgroundImageView.frame.height / 10
        let position = CGSize(width: 0, height: height)
        self.middleBackgroundImageView.roundCorners(radius: radius)
        self.middleScreenView.setRadiusWithShadow(radius: radius, position: position, opacity: 0.5)
        self.additionalView.roundCorners(radius: self.additionalView.frame.height / 2)
        self.weatherView.roundCorners(radius: self.additionalView.frame.height / 2)
    }
    
}

extension ViewController: FavoritesViewControllerDelegate  {
    func addFavoriteCity(with object: WeatherResponse?, or city: String?) {
        if let city = object?.location.name {
            self.city = city
        } else {
            self.city = city!
        }
        self.sendCityRequest()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast?.forecast.forecastday.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ForecastTableViewCell", for: indexPath) as? ForecastTableViewCell else {
            return UITableViewCell()
        }
    
        if let date = Manager.shared.json?.forecast.forecastday[indexPath.row].date,
           let icon = Manager.shared.json?.forecast.forecastday[indexPath.row].day?.condition?.icon?.dropFirst(2),
           let date_epoch = Manager.shared.json?.forecast.forecastday[indexPath.row].date_epoch,
           let maxtemp_c = Manager.shared.json?.forecast.forecastday[indexPath.row].day?.maxtemp_c,
           let mintemp_c = Manager.shared.json?.forecast.forecastday[indexPath.row].day?.mintemp_c {
            
            let localDate = Date(timeIntervalSince1970: date_epoch)
            let url = URL(string: "https://" + icon)
            
            cell.dateLabel.text = date
            cell.dayLabel.text = localDate.getDay()
            cell.weatherPictureImageView.kf.setImage(with: url)
            cell.maxTempLabel.text = "\(Int(maxtemp_c))°"
            cell.minTempLabel.text = "\(Int(mintemp_c))°"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.height / 10
    }
    
    
}


